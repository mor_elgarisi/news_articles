import { makeAutoObservable,toJS } from "mobx";
import { makePersistable,
  clearPersistedStore,
  getPersistedStore,
  stopPersisting,
  isHydrated,
  isPersisting,
  hydrateStore,
  startPersisting,
  pausePersisting } from "mobx-persist-store";
import { AsyncStorage } from 'react-native';
import _ from 'lodash'
class ArticlesStore {
  constructor() {
    makeAutoObservable(this)
   // persistStore(this, ['username'], 'UserStore');
    makePersistable(this, { name: 'ArticlesStore',
     properties: ['articles'],
      storage:AsyncStorage });

  }

  articles = []

  addArticle(article) {
    let found = _.findIndex(this.articles, o => {
      return o.title== article.title;
    });

    if(found!==-1){

    let new_articles= _.remove(this.articles, o => {
      return o.title!== article.title;
    }); 
    this.articles=new_articles
    }else{
    this.articles.push(article)
    }
  }

  get Articles() {
    return this.articles;
  }

   CheckIfFav(article){
    let found = _.find(this.articles, o => {
      return o.title== article.title;
    });
    if(found)
    return true
    return false
  }

  async clearStore() {
    await clearPersist(this);
  }

  stopPersist() {
    stopPersist(this);
  }

  startPersist() {
    startPersist(this);
  }

  get isHydrated() {
    return isHydrated(this);
  }

}

const articlesStore = new ArticlesStore();
export default articlesStore;
