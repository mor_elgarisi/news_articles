import { makeAutoObservable } from "mobx";
import { makePersistable,
  clearPersistedStore,
  getPersistedStore,
  stopPersisting,
  isHydrated,
  isPersisting,
  hydrateStore,
  startPersisting,
  pausePersisting } from "mobx-persist-store";
import { AsyncStorage } from 'react-native';

class UserStore {
  constructor() {
    makeAutoObservable(this)
    makePersistable(this, { name: 'UserStore',
     properties: ['user','token'],
      storage:AsyncStorage });

  }

  user = null
  token = null

  setUser(user) {
    this.user = user
  }

  get isLogin() {
    return  this.user !=null;
  }
  get getUsername() {
    return this.username;
  }

  get getUserColor() {
    return this.color;
  }

  async clearStore() {
    await clearPersist(this);
  }

  stopPersist() {
    stopPersist(this);
  }

  startPersist() {
    startPersist(this);
  }

  get isHydrated() {
    return isHydrated(this);
  }

}

const userStore = new UserStore();
export default userStore;
