import { StyleSheet, Dimensions ,} from 'react-native'

const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        width: '100%'

    },
    linearGradient: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 50
    },
    logo: {
        width: 230,
        height: 100,
        alignSelf: 'center',
        justifyContent: 'center',
        resizeMode: 'contain'
    },
    text: {
        fontSize: 21,
        color: 'black',
        fontFamily: 'Avenir-Heavy',
        textAlign: 'center'
    },
    containerEmpty: {
        flex: 1,
        width: Dimensions.get('window').width,
    },
    contentContainer: {
        alignItems: 'center',
        paddingBottom:30,
        flexGrow: 1 
    }

})

export default style
