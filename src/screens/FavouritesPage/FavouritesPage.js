import React from 'react';
import {View,Text,FlatList,Image,Dimensions} from 'react-native';
import style from './style'
import { ArticleItem, Header, } from '../../components'
import { observer, inject } from "mobx-react";
import LinearGradient from 'react-native-linear-gradient';

export default FavouritesPage = inject('ArticlesStore', 'UserStore')(observer((props) => {

  const renderArticle = (item) => <ArticleItem item={item.item} />
  
  const renderHeader = () => {
    if (props.ArticlesStore.Articles.length > 0)
      return (<Header text={'Articles You Liked'} />)
    return null
  }

  const renderEmpty = () => {
    return (
      <View style={style.containerEmpty}>
        <LinearGradient
          start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
          colors={['#f6d365', '#fda085']}
          style={style.linearGradient}
        >
          <Image
            style={style.logo}
            source={require('../../assets/Dashboard/logo.png')}
            resizeMode="contain"
          />
          <Text style={style.text}>Please add some Articles To List</Text>
        </LinearGradient>
      </View>
    )
  }

  return (
    <View style={[style.container]}>
      <FlatList
        contentContainerStyle={[style.contentContainer]}
        ListHeaderComponentStyle={{ width: Dimensions.get('window').width }}
        horizontal={false}
        data={props.ArticlesStore.Articles}
        renderItem={renderArticle}
        keyExtractor={(item, index) => index}
        horizontal={false}
        ListEmptyComponent={renderEmpty}
        ListHeaderComponent={renderHeader}
      />
    </View>
  )


}))
