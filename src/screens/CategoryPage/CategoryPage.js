import React, { useEffect, useState } from 'react';
import { View, Text, FlatList } from 'react-native';
import { Placeholder, PlaceholderMedia, PlaceholderLine, Fade } from "rn-placeholder";
import Api from '../../utils/Api';
import style from './style'
import { ArticleItem } from '../../components'
import { observer, inject } from "mobx-react";

export default CategoryPage = inject('ArticlesStore', 'UserStore')(observer((props) => {

  const { category } = props.route.params || ''
  const [articels, setArticles] = useState(null);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    try {
      setLoading(true);
      let data = {
        categories: category.key,
        countries: "us",
      }
      let category_articles = await Api.getCategories(data)
      setArticles(category_articles.data);
      setLoading(false);


    } catch (error) {
      alert(error)
    }
  }
  const renderArticle = (item) => <ArticleItem item={item.item} />

  return (
    <View style={[style.container]}>

      {loading ?
      <View style={{padding:10}}>
        <Placeholder
          Animation={Fade}
          Left={PlaceholderMedia}
        >
          <PlaceholderLine width={80} />
          <PlaceholderLine />
          <PlaceholderLine />
          <PlaceholderLine />
          <PlaceholderLine width={30} />
          <PlaceholderLine width={80} />
          <PlaceholderLine width={80} />

        </Placeholder> 
        </View>
        : <FlatList
          horizontal={false}
          data={articels}
          renderItem={renderArticle}
          keyExtractor={(item, index) => index}
          horizontal={false}
          ListEmptyComponent={() => <View><Text>oops! no articles on this category</Text></View>}
        />

      }

    </View>
  )


}))
