import { StyleSheet,Platform } from 'react-native'
const style = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: 'center',
    },
    linearGradient: {
        flex: 1,
        width: 'auto',
       // justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 20
    },
    logo: {
        width: 230, 
        height: 100, 
        alignSelf: 'center',
        justifyContent:'center',
        marginTop:Platform.OS=='ios'?20:0
    },
    text:{
        color:'white',
        fontFamily: 'Avenir-Heavy',
        fontSize:24,
        textAlign:'center',
        width:'60%',
    },
    logo:{
        width: 200, 
        height: 100, 
        resizeMode:'contain'
    },
    fbContainer:{
        paddingTop:20
    },
    fbbtn:{
        width:200,
        height:50,
        backgroundColor:'blue',
        borderRadius:50,
        alignItems:'center',
        justifyContent:'center',
        
    },
    fbtext:{
        textAlign:'center',
        color:'white',
        fontWeight:'bold'
    }

})

export default style
