import React, { Component } from 'react';
import {
  View,
  Image,
  Text
} from 'react-native';
import { Button } from '../../components'
import { LoginButton, AccessToken, LoginManager } from 'react-native-fbsdk-next';
import style from './style'
import LinearGradient from 'react-native-linear-gradient';
import { observer, inject } from "mobx-react";
import { useNavigation } from "@react-navigation/native";
import { CommonActions } from '@react-navigation/native';
import auth from '@react-native-firebase/auth';



export default FavBtn = inject('UserStore', 'ArticlesStore')(observer((props) => {

  const navigation = useNavigation();

  const { item } = props.route.params || null


  async function onFacebookButtonPress() {
    // Attempt login with permissions
    const result = await LoginManager.logInWithPermissions(['public_profile']);

    if (result.isCancelled) {
      throw 'User cancelled the login process';
    }

    // Once signed in, get the users AccesToken
    const data = await AccessToken.getCurrentAccessToken();

    if (!data) {
      throw 'Something went wrong obtaining access token';
    }

    // Create a Firebase credential with the AccessToken
    const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);

    // Sign-in the user with the credential
    let res = auth().signInWithCredential(facebookCredential);
    if (res) {
      props.UserStore.setUser(data)
      if (item) {
        props.ArticlesStore.addArticle(item)
      }

      navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [
            { name: 'Dashboard' },
            { name: 'FavouritesPage' },
          ]
        }));
    }
  }

  return (
    <View style={[style.container]}>
      <LinearGradient
        start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
        colors={['#f6d365', '#fda085']}
        style={style.linearGradient}>
        <Button onPress={() => navigation.goBack()} title="Dismiss" />
        <Image style={style.logo} source={require('../../assets/Dashboard/logo.png')} />
        <Text style={style.text}>Please Login To See Your Favourits!</Text>
        <View style={style.fbContainer}>
          <Button onPress={() => onFacebookButtonPress()}>
            <View style={style.fbbtn}>
              <Text style={style.fbtext}>facebook login</Text>
            </View>
          </Button>
        </View>
      </LinearGradient>
    </View>

  )

}))
