import { StyleSheet } from 'react-native'

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',

  },
  text: {
    color: 'black',
    fontSize: 45
  },
  linearGradient: {
    flex: 1,
    width: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 20
  }

})

export default style
