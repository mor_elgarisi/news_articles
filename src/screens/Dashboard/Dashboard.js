import React from 'react';
import {View,FlatList,} from 'react-native';
import style from './style'

import { CategoryItem ,Header} from '../../components'


export default Dashboard = () => {
  const allCategories = [
    {
      id: 0,
      title: "General",
      key: "general",
    },
    {
      id: 1,
      title: "Business",
      key: "business",
    },
    {
      id: 2,
      title: "Entertainment",
      key: "entertainment",
    },
    {
      id: 3,
      title: "Health",
      key: "health",
    },
    {
      id: 4,
      title: "Science",
      key: "science",
    },
    {
      id: 5,
      title: "Sports",
      key: "sports",

    },

    {
      id: 6,
      title: "Technology",
      key: "technology",

    },
    {
      id: 7,
      title: "Favourits",
      key: "favourits",

    },
  ]


  const renderCategoryItem = ({ item }) => <CategoryItem item={item} />
  return (
    <View style={[style.container]}>
      <Header/>
      <FlatList
        horizontal={false}
        data={allCategories}
        renderItem={renderCategoryItem}
        keyExtractor={item => item.title}
        numColumns={2}
        horizontal={false}
      />

    </View>
  );
};