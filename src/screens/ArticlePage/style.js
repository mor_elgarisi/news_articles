import { StyleSheet, Dimensions } from 'react-native'

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  imageWrapper: {
    borderWidth: StyleSheet.hairlineWidths,
    borderColor: 'black',
  },
  image: {
    height: 200,
    width: Dimensions.get('window').width,
    resizeMode: 'stretch',
  },
  defaultImage: {
    height: 200,
    width: Dimensions.get('window').width,
    resizeMode: 'contain',
    opacity: 0.5
  },
  title: {
    color: 'black',
    textAlign: 'left',
    fontFamily: 'Avenir-Heavy',
    fontSize: 26,
    lineHeight: 26,
    paddingTop: 10,
    paddingHorizontal: 10,
  },
  titleWrapper: { 
    paddingTop: 5,
    paddingHorizontal: 20
  },
  des: {
    color: 'black',
    textAlign: 'left',
    fontFamily: 'Avenir-Book',
    fontSize: 15
  },
  detailsViewWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  details: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingVertical: 15,
  },
  profileImage: {
    height: 30,
    width: 30,
    marginRight: 10
  },
  author: {
    color: 'black',
    fontFamily: 'Avenir-Heavy',
    fontSize: 13
  },
  publishDate: {
    color: '#aaaaaa',
    textAlign: 'left',
    fontFamily: 'Avenir-Book',
    fontSize: 12
  },
  favBtn: {
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: '#aaaaaa',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    padding: 10
  },
  source: {
    flexDirection: 'row'
  }

})

export default style
