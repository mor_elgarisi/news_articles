import React, { useState, useEffect } from 'react';
import { ScrollView, View, Text, Image } from 'react-native'
import style from './style'
import { CategoryTag, FavBtn } from '../../components'
import moment from 'moment'

export default ArticlePage = (props) => {

    const { item } = props.route.params
    return (
        <ScrollView style={style.container}>
            <View style={style.imageWrapper}>
                {item.image ?
                    <Image source={{ uri: item.image }} style={style.image} /> :
                    <Image source={require('../../assets/Dashboard/logo.png')} style={style.defaultImage} resizeMode={'contain'} />
                }
            </View>
            <Text style={style.title}>{item.title}</Text>
            <View style={style.titleWrapper}>
                <View style={style.detailsViewWrapper}>
                    <View style={style.details}>
                        <View>
                            <Image source={require('../../assets/Dashboard/profile2.png')} style={style.profileImage} />
                        </View>
                        <View>
                            <Text style={style.author}>{item.author}</Text>
                            <Text style={style.publishDate}>{moment(item.published_at).format('MMMM Do YYYY, h:mm:ss a')}</Text>
                        </View>
                    </View>
                    <FavBtn item={item} />
                </View>
                <View style={style.source}>
                    <CategoryTag category_name={item.category} />
                    <CategoryTag text={item.source.trim()} />

                </View>
                <Text style={style.des}>{item.description}</Text>
            </View>
            <View style={{ height: 50 }} />

        </ScrollView>


    )

};