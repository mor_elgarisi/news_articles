import Dashboard from './Dashboard'
import Login from './Login'
import FavouritesPage from './FavouritesPage'
import CategoryPage from './CategoryPage'
import ArticlePage from './ArticlePage'
export { Dashboard ,Login,FavouritesPage,CategoryPage,ArticlePage}
