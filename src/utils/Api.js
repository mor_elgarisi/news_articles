import Rest from './Rest'
class Api {
  
    getCategories(params) {
        return Rest.send("news", "GET", null, params);
      }
}

export default new Api()
