class Rest {


    constructor() {
       this.API_URL = 'http://api.mediastack.com/v1/';
       this.API_KEY = 'ee2a8868c74cee34ebe7b5ec85f29a07'
    }
    
    withQuery(url, params) {
      let query = Object.keys(params)
        .filter(k => !!params[k])
        .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`)
        .join('&');
      url += (url.indexOf('?') === -1 ? '?' : '&') + query;
      return url+'&access_key='+this.API_KEY;
    }
  
    async send(path, method, body, params) {
  
      const headers = {};
      let responseJson;
      headers['Accept'] = 'application/json';
      headers['Content-Type'] = 'application/json';
      let url = this.API_URL + path;
  
      if (method == 'GET') {
        url = this.withQuery(url, params);
      }
      try {
        // let response;
       let response = await fetch(url, {
          method: method,
          headers: headers,
          body: body != null ? JSON.stringify(body) : null,
          timeout: 10000,
        });
  
        if (response.bodyString) {
          responseJson = JSON.parse(response.bodyString);
        } else {
          responseJson = response.json();
        }
  
        return responseJson;
      } catch (error) {
        console.log(error)
        alert(error);
      }
    }
  }
  
  const rest = new Rest();
  
  export default rest;
  