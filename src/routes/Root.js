import React,{useState} from 'react';
import {
    StatusBar,
    Text,
    View,
} from 'react-native';
import { Provider, observer } from 'mobx-react';
import { UserStore,ArticlesStore } from '../stores'
import { NavigationContainer } from '@react-navigation/native';
import DashboardStack from './Dashboardstack'
import {Splash} from '../components'
import { LoginButton, AccessToken,LoginManager } from 'react-native-fbsdk-next';

const stores = { UserStore,ArticlesStore };



const Root = observer(() => {
    const [ready, setReady] = useState(false);  
    LoginManager.setLoginBehavior('web_only')
    const allStoreAreSynchronized = () => {
        return Object.values(stores).every((store => {
            console.log('hydrate', store.isHydrated)
            return store.isHydrated
        }))
    }
 
        if (!allStoreAreSynchronized()) {
            setTimeout(() => {
                setReady(true);
            }, 2000);
               
        }
  
if(!ready)
return <Splash/>
    return (
        <View  style={{flex:1}}>
            <View style={{flex:1}}>
            <StatusBar barStyle={'light-content'} translucent={false} hidden={false}/>
            <Provider {...stores}>
                <NavigationContainer>
                    <DashboardStack />
                </NavigationContainer>

            </Provider>
            </View>
        </View>
    );
})

export default Root;
