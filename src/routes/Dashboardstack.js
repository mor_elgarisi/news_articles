import React, { useEffect } from "react";
import { Observer, inject } from "mobx-react";

import { Text, View, Image,Dimensions } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";

import { observer } from "mobx-react";
import { Dashboard, CategoryPage, FavouritesPage, Login, ArticlePage } from "../screens";
import Icon from 'react-native-vector-icons/AntDesign';

const App = createStackNavigator();

export const DashboardStack = inject('UserStore')(observer(() => {
    Dimensions.get('window').width
    return (
        <App.Navigator
            initialRouteName={'Dashboard'}
        >
            <App.Screen
                name="Dashboard"
                component={Dashboard}
                options={{
                    headerShown:false
                }}
            />
            <App.Group
                screenOptions={({ navigation }) => ({
                    headerLeft: () => <Icon name="left" size={30} color="black" style={{ paddingLeft: 10 }} onPress={navigation.goBack} />,
                    title:()=>''
                })}
            >
                <App.Screen name="CategoryPage" component={CategoryPage} />
                <App.Screen name="FavouritesPage" component={FavouritesPage} /> 
                <App.Screen name="ArticlePage" component={ArticlePage} />

            </App.Group>
            <App.Group
                screenOptions={({ navigation }) => ({
                    presentation: 'modal',
                    headerLeft: () => <Icon name="closecircleo" size={30} color="black" style={{ paddingLeft: 10 }} onPress={navigation.goBack} />,
                })}
            >
                <App.Screen name="Login" component={Login} />
            </App.Group>
        </App.Navigator>
    );
}));

export default DashboardStack;
