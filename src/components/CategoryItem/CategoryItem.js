import React, { useState, useEffect } from 'react';
import { Platform, View, Text, Image } from 'react-native'
import LinearGradient from 'react-native-linear-gradient';
import { useNavigation } from "@react-navigation/native";
import style from './style'
import { Button } from '../../components'
import { observer, inject } from "mobx-react";
import { UserStore } from '../../stores'



export default CategoryItem = observer((props) => {

  const { item } = props
  const navigation = useNavigation();

  const images = [
    { image: require('../../assets/Dashboard/general.png') },
    { image: require('../../assets/Dashboard/bus.png') },
    { image: require('../../assets/Dashboard/ente.png') },
    { image: require('../../assets/Dashboard/health.png') },
    { image: require('../../assets/Dashboard/scie.png') },
    { image: require('../../assets/Dashboard/sport.png') },
    { image: require('../../assets/Dashboard/tech.png') },
    { image: require('../../assets/Dashboard/Fav.png') },

  ];
  const goToCategoryResult = () => {
    if (item.key == 'favourits') {
      if (UserStore.isLogin) {
      navigation.navigate("FavouritesPage");
      } else {
        navigation.navigate("Login",{action:'navigate'});
      }
    } else {
      navigation.navigate("CategoryPage", { category: item });
    }
  };

  return (

    <Button onPress={
      () => goToCategoryResult()
    }>
      <View style={Platform.OS == 'ios' ? style.container_ios : style.container_android}>
        <LinearGradient
          start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
          colors={['#ffd700', '#f5fffa']}
          style={style.circle}
        >
          <Image style={style.image} source={images[item.id].image} />
        </LinearGradient>
        <Text style={style.text}>{item.title}</Text>
      </View>
    </Button>

  );
});
