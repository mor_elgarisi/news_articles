import { StyleSheet, Platform, Dimensions } from 'react-native'

const style = StyleSheet.create({
  container_ios: {
    shadowRadius: 3,
    shadowOpacity: 0.40,
    shadowOffset: { width: 0, height: 3 },
    borderRadius: 10,
    backgroundColor: 'white',
    width: Dimensions.get('window').width / 2 - 20,
    margin: 10,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    height: 150
  },
  container_android: {
    elevation: 3,
    borderRadius: 5,
    backgroundColor: 'white',
    width: Dimensions.get('window').width / 2 - 20,
    margin: 10,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    height: 150
  },
  text: {
    color: 'black',
    fontSize: 20,
    fontFamily: 'Avenir-Heavy',
    paddingBottom: 10,
    position: 'absolute',
    bottom: 0
  },
  image: {
    height: 50,
    width: 52,
    resizeMode: 'contain'

  },
  linearGradient: {
    flex: 1,
    width: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 10,
    borderRadius: 10,
  },
  circle: {
    width: 100,
    height: 100,
    borderRadius: 50,
    zIndex: -1,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center'
  }

})

export default style
