import { StyleSheet, Platform, Dimensions } from 'react-native'

const style = StyleSheet.create({

    container: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 3
    },
    text: {
        color: 'black',
        fontSize: 12,
        fontFamily: 'Avenir-Heavy',
        padding:5,
        top:3,
        textTransform: 'capitalize'
    },
    image: {
        height: 20,
        width: 20,
        resizeMode: 'contain',

    },



})

export default style
