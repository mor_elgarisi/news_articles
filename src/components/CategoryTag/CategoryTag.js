import React from 'react';
import {  View, Text, Image } from 'react-native'
import style from './style'



export default CategoryTag = (props) => {


    const images = {
        'general': require('../../assets/Dashboard/general.png'),
        'business': require('../../assets/Dashboard/bus.png'),
        'entertainment': require('../../assets/Dashboard/ente.png'),
        'health': require('../../assets/Dashboard/health.png'),
        'science': require('../../assets/Dashboard/scie.png'),
        'sports': require('../../assets/Dashboard/sport.png'),
        'technology': require('../../assets/Dashboard/tech.png'),

    }
    return (
        <View style={style.container}>
            <View>
                <Image style={style.image} source={props.category_name?images[props.category_name]:require('../../assets/Dashboard/location.png')} />
            </View>
            <View style={{width:props.category_name?null:'84%'}}>
                <Text numberOfLines={1} style={style.text}>{props.category_name? props.category_name:props.text}</Text>
            </View>
        </View>

    );
};