import { StyleSheet,Platform } from 'react-native'
const style = StyleSheet.create({
    container: {
        justifyContent: 'center',
        flex:1
    },
    linearGradient: {
        flex: 1,
        width: 'auto',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 20
    },
    logo: {
        width: 230, 
        height: 200, 
        alignSelf: 'center',
        justifyContent:'center',
        marginTop:Platform.OS=='ios'?20:0,
         resizeMode:'contain'
    },
 

})

export default style
