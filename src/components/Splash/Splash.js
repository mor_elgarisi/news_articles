import React, {Component } from 'react';
import {View, Animated} from 'react-native';
import style from './style'
import LinearGradient from 'react-native-linear-gradient';




export default class Splash extends Component {

    constructor(props) {
        super(props)
        this.state = {}
        this.springValue = new Animated.Value(0.3)

    }

    componentDidMount() {
        this.spring()
    }
    spring = () => {
        this.springValue.setValue(0.3)
        Animated.spring(
            this.springValue,
            {
                toValue: 1,
                friction: 1,
                useNativeDriver: true
            }
        ).start()
    }

    render() {
        return (
            <View style={[style.container]}>
                <LinearGradient
                    start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
                    colors={['#f6d365', '#fda085']}
                    style={style.linearGradient}>
                    <Animated.Image
                        style={[style.logo, { transform: [{ scale: this.springValue }] }]}
                        source={require('../../assets/Dashboard/logo2.png')} />
                </LinearGradient>
            </View>
        )
    }
}
