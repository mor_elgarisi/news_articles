import { StyleSheet, Platform, Dimensions } from 'react-native'
const style = StyleSheet.create({
  container_ios: {
    shadowRadius: 3,
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 3 },
    borderRadius: 10,
    backgroundColor: 'white',
    width: Dimensions.get('window').width - 40,
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center',

  },
  container_android: {
    elevation: 3,
    borderRadius: 8,
    backgroundColor: 'white',
    width: Dimensions.get('window').width - 40,
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageWrapper: {
    paddingTop: 5
  },
  image: {
    height: 200,
    width: Dimensions.get('window').width - 45,
    resizeMode: 'stretch',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,

    //flex:1

  },
  title: {
    color: 'black',
    textAlign: 'left',
    fontFamily: 'Avenir-Heavy',
    fontSize: 14

  },
  titleWrapper: {
    padding: 10
  },
  des: {
    color: 'black',
    textAlign: 'left',
    fontFamily: 'Avenir-Book',
    fontSize: 12
  },
  detailsViewWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // paddingVertical:15
  },
  details: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingVertical: 15,
  },
  profileImage: {
    height: 30,
    width: 30,
    marginRight: 10
  },
  author: {
    color: 'black',
    fontFamily: 'Avenir-Heavy',
    fontSize: 13
  },
  publishDate: {
    color: '#aaaaaa',
    textAlign: 'left',
    fontFamily: 'Avenir-Book',
    fontSize: 12
  },
  favBtn: {
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: '#aaaaaa',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    padding: 10
  }


})

export default style
