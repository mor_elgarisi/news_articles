import React, { useState, useEffect } from 'react';
import { Platform, View, Text, Image, ImageBackground } from 'react-native'
import { useNavigation } from "@react-navigation/native";
import style from './style'
import { Button,FavBtn } from '../../components'
import Icon from 'react-native-vector-icons/EvilIcons';
import moment from 'moment'


export default ArticleItem = (props) => {

    const { item } = props
    const navigation = useNavigation();
    const goToArticlePage = () => {
        navigation.navigate('ArticlePage', { item: item })
    }
    return (

        <Button onPress={() => goToArticlePage()}>
            <View style={Platform.OS == 'ios' ? style.container_ios : style.container_android}>
                {item.image && <View style={style.imageWrapper}>
                    <Image source={{ uri: item.image }} style={style.image} />
                </View>
                }
                <View style={style.titleWrapper}>
                    <Text numberOfLines={2} style={style.title}>{item.title}</Text>
                    <Text numberOfLines={3} style={style.des}>{item.description}</Text>
                    <View style={style.detailsViewWrapper}>
                        <View style={style.details}>
                            <View>
                                <Image source={require('../../assets/Dashboard/profile2.png')} style={style.profileImage} />
                            </View>
                            <View>
                                <Text style={style.author}>{item.author}</Text>
                                <Text style={style.publishDate}>{moment(item.published_at).format('MMMM Do YYYY, h:mm:ss a')}</Text>
                            </View>
                        </View>
                       <FavBtn item={item}/>
                    </View>
                </View>
            </View>
        </Button>


    )

};