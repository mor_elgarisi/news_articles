import { StyleSheet,Platform } from 'react-native'
const style = StyleSheet.create({
    container: {
        height: 100,
        justifyContent: 'center',
    },
    linearGradient: {
        flex: 1,
        width: 'auto',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 20
    },
    logo: {
        width: 230, 
        height: 100, 
        alignSelf: 'center',
        justifyContent:'center',
        marginTop:Platform.OS=='ios'?20:0,
         resizeMode:'contain'
    },
    text:{
        fontSize:24,
        color:'black',
        fontFamily: 'Avenir-Heavy',
        textAlign:'center'
    }

})

export default style
