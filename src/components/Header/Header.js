import React, { useState, useEffect } from 'react';
import {View,Image,Text} from 'react-native';
import style from './style'
import LinearGradient from 'react-native-linear-gradient';





export default Header = (props) => {
  return (
    <View style={[style.container]}>
      <LinearGradient
        start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
        colors={['#f6d365', '#fda085']}
        style={style.linearGradient}>
      { props.text?
      <Text style={style.text}>{props.text}</Text>:
       <Image
          style={style.logo}
          source={require('../../assets/Dashboard/logo.png')}
          resizeMode="contain"
        />}
      </LinearGradient>
    </View>
  );
};