import React from 'react';
import { View } from 'react-native'
import { useNavigation } from "@react-navigation/native";
import style from './style'
import { Button } from '../../components'
import Icon from 'react-native-vector-icons/EvilIcons';
import { observer, inject } from "mobx-react";


export default FavBtn = inject('ArticlesStore','UserStore')( observer( (props) => {

    const navigation = useNavigation();

    const addToFav = () => {
        if (props.UserStore.isLogin ) {
        props.ArticlesStore.addArticle(props.item)
        }else{
            navigation.navigate('Login',{item:props.item})
        }
    }

    const active=props.ArticlesStore.CheckIfFav(props.item) && props.UserStore.isLogin
    return (
        <Button onPress={()=>addToFav()}>
            <View style={active?style.active:style.favBtn}>
                <Icon name="heart" size={30} color={active?'red':"#aaaaaa"} />
            </View>
        </Button>



    )

}))