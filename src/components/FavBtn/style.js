import { StyleSheet, Platform, Dimensions } from 'react-native'

const style = StyleSheet.create({
  favBtn: {
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: '#aaaaaa',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    padding: 5
  },
  active: {
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    padding: 5
  },


})

export default style
