import React from "react"
import { Platform, TouchableOpacity, TouchableNativeFeedback, View } from 'react-native'


function _handlePress(callback) {
  requestAnimationFrame(callback)
}
const Button = props => {
  return Platform.OS === 'ios' ? (
    <TouchableOpacity activeOpacity={0.6} disabled={props.disabled} style={[props.style, { opacity: props.disabled ? 0.5 : 1 }]} onPress={e => _handlePress(props.onPress)}>
      {props.children}
    </TouchableOpacity>
  ) : (
    <TouchableNativeFeedback useForeground={true} background={TouchableNativeFeedback.Ripple(props.rippleColor || 'white', false)} disabled={props.disabled} onPress={e => _handlePress(props.onPress)}>
      <View style={[props.style, { opacity: props.disabled ? 0.5 : 1 }]}>{props.children}</View>
    </TouchableNativeFeedback>
  )
  // return(<View></View>)
}

Button.defaultProps = {
  onPress: () => {},
}


export default Button
